<?php
class M_Admin extends CI_Model{

	function get_data($table){
		return $this->db->get($table);
	}

	function insert_data($data,$table){
		$this->db->insert($table,$data);
	}
	
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	
	function delete_data($where,$table){
		$this->db->delete($table,$where);
	}

	function pilih_data($where,$table){		
		return $this->db->get_where($table,$where);
	}

	public function insert($data)
	{
		$this->db->insert('users',$data);
		return TRUE;
	}

	function get_dataPost($id)
	{
		$this->db->join('account b', 'b.username = a.username');
		$this->db->where('idpost', $id);
		return $this->db->get('post a');
	}
	
	function get_dataAkun($id)
	{
		$this->db->where('username', $id);
		return $this->db->get('account');
	}

	function get_dataStokLimit()
	{
		$this->db->join('data_barang b', 'b.id_barang = a.id_barang');
		$this->db->order_by('a.id_stok', 'DESC');
		$this->db->limit(5);
		return $this->db->get('stok a');
	}

	function get_dataPenjualanLimit()
	{
		$this->db->join('data_barang b', 'b.id_barang = a.id_barang');
		$this->db->order_by('a.id_penjualan', 'DESC');
		$this->db->limit(5);
		return $this->db->get('penjualan a');
	}

	function get_dataPenjualan()
	{
		$this->db->join('data_barang b', 'b.id_barang = a.id_barang');
		$this->db->join('user c', 'c.id_user = a.id_user');
		$this->db->order_by('tgl', 'DESC');
		return $this->db->get('penjualan a');
	}

	function get_dataPenjualanCari($tgl)
	{
		$this->db->join('data_barang b', 'b.id_barang = a.id_barang');
		$this->db->where('tgl', $tgl);
		return $this->db->get('penjualan a');
	}

	function updateBarang($id, $jumlah){
		return $this->db->query("UPDATE `data_barang` SET `jumlah_barang` = '$jumlah' WHERE `data_barang`.`id_barang` = '$id'");
	}

	function get_dataBarang($id)
	{
		$this->db->from('data_barang');
		$this->db->select('*');
		$this->db->where('id_barang', $id);
		return $this->db->get();
	}

	function getDataPegawai($id)
	{
		$this->db->from('data_pegawai');
		$this->db->select('*');
		$this->db->where('id_pegawai', $id);
		return $this->db->get();
	}

	function get_dataSuplierEdit($id)
	{
		$this->db->from('suplier');
		$this->db->select('*');
		$this->db->where('id_suplier', $id);
		return $this->db->get();
	}	

	function get_dataStokEdit($id)
	{
		$this->db->from('stok');
		$this->db->select('*');
		$this->db->where('id_stok', $id);
		return $this->db->get();
	}

	function get_jumlahStok()
	{
		$sql = "SELECT * FROM `data_barang` WHERE jumlah_barang<=jum_min";
		return $result = $this->db->query($sql);
	}

	function get_jumlahStokNotifikasi(){
		$sql = "SELECT COUNT(id_barang) AS jumlah FROM data_barang WHERE jumlah_barang<=jum_min";
		$result = $this->db->query($sql);
		return $result->row()->jumlah;
	}

	function updateKonfirmasi($id)
	{
		$sql = "UPDATE `data_barang` SET `konfirmasi_suplier` = '1' WHERE `data_barang`.`id_barang` = $id";
		return $result = $this->db->query($sql);
	}

	function updatekonSuplier($id)
	{
		$sql = "UPDATE `data_barang` SET `konfirmasi_suplier` = '0' WHERE `data_barang`.`id_barang` = $id";
		return $result = $this->db->query($sql);
	}

	function get_barangLimit()
	{
		$sql = 'SELECT * FROM `data_barang` WHERE jumlah_barang<=jum_min';
		return $result = $this->db->query($sql);
	}

}