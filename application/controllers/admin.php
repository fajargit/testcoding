<?php
class Admin extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('M_admin');

    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }

  function index(){
    //Allowing akses to admin only
      $data['dataPost'] = $this->M_admin->get_data('post')->result();
      $this->load->view('admin/data',$data);
    
  }


  function tambahPost()
  {
    $this->load->view('admin/addpost');
  }

  function aksi_tambahPost(){
    $id        = "";
    $cek = $_SESSION["username"];
    $title      = $this->input->post('title');
    $content     = $this->input->post('content');
    $date    = $this->input->post('date');
    $username    = $this->input->post('username');

    $data     = array('idpost'=>$id, 'title'=>$title, 'content'=>$content, 'date'=>$date, 'username'=>$cek);
    $this->M_admin->insert_data($data,'post');
    $this->session->set_flashdata('flash','Ditambah');
    redirect(base_url().'admin');
  }

  function hapusPost($id){
    $where = array('idpost'=>$id);
    $this->M_admin->delete_data($where,'post');
    redirect(base_url().'admin/index');
  }

  function editPost($id)
  {
    $data['dataPost'] = $this->M_admin->get_dataPost($id)->result();
    $this->load->view('admin/edit',$data);
  }


  function aksi_editPost()
  {
    $id        = $this->input->post('idpost');
    $cek = $_SESSION["username"];
    $title      = $this->input->post('title');
    $content     = $this->input->post('content');
    $date    = $this->input->post('date');
    $username    = $this->input->post('username');

    $data     = array('idpost'=>$id, 'title'=>$title, 'content'=>$content, 'date'=>$date, 'username'=>$cek);
    $where = array(
      'idpost' => $id
    );
    $this->M_admin->update_data($where, $data, 'post');
    redirect(base_url().'admin/index');
  }

  function akun(){
    $data['dataAkun'] = $this->M_admin->get_data('account')->result();
    $this->load->view('admin/akun', $data);
  }

  function tambahAkun()
  {
    $this->load->view('admin/addAkun');
  }

  function aksi_tambahAkun()
  {
    $username      = $this->input->post('username');
    $password     = $this->input->post('password');
    $name    = $this->input->post('name');
    $role    = $this->input->post('role');


    $data     = array('username'=>$username, 'password'=>md5($password), 'name'=>$name, 'role'=>$role);
    $this->M_admin->insert_data($data,'account');
    $this->session->set_flashdata('flash','Ditambah');
    redirect(base_url().'admin/akun');
  }

  function editAkun($id)
  {
    $data['dataAkun'] = $this->M_admin->get_dataAkun($id)->result();
    $this->load->view('admin/editAkun',$data);
  }

  function aksi_editAkun()
  {
    $username      = $this->input->post('username');
    $password     = $this->input->post('password');
    $name    = $this->input->post('name');
    $role    = $this->input->post('role');

    $data     = array('username'=>$username, 'password'=>md5($password), 'name'=>$name, 'role'=>$role);
    $where = array(
      'username' => $username
    );
    $this->M_admin->update_data($where, $data, 'account');
    redirect(base_url().'admin/akun');
  }

  function hapusAkun($id){
    $where = array('username'=>$id);
    $this->M_admin->delete_data($where,'account');
    redirect(base_url().'admin/akun');
  }



  

}
