<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function login_aksi()
	{
		$username    = $this->input->post('username',TRUE);
		$password = md5($this->input->post('password',TRUE));
		$validate = $this->M_login->validate($username,$password);
		if($validate->num_rows() > 0){
			$data  		= $validate->row_array();
			$username	= $data['username'];
			$nama		= $data['name'];
			$role		= $data['role'];
			$cek		= $data['cek'];
			$sesdata = array(
				'username'    => $username,
				'nama'        => $nama,    
				'role'        => $role,    
				'logged_in'   => TRUE
			);
			$this->session->set_userdata($sesdata);
            // access login for admin
			if($role === 'Admin'){
				redirect('admin');

            // access login for staff
			}elseif($role === 'Author'){
				redirect('admin');

            // access login for sampling
			}
		}else{
			redirect(base_url().'login?alert=gagal');
			redirect('login');
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url().'login?alert=logout');
		redirect('login');
	}
}
