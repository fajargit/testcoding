<?php $this->load->view('layout/sidebar'); ?>
<?php $this->load->view('layout/header'); ?>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 mb-4">
                    <div class="overview-wrap">
                        <h2 class="title-1">Data Post</h2>
                            <a href="<?= base_url('admin/tambahPost/') ?>"><button class="au-btn au-btn-icon au-btn--green">
                                <i class="zmdi zmdi-plus"></i> Tambah Post</button></a>
                        </div>
                    </div>
                </div>

                <div class="flash-data" data-flash=" <?= $this->session->flashdata('flash') ?>" ></div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive table--no-card m-b-40">
                                    <table id="table_id" class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Title</th>
                                                <th>Konten</th>
                                                <th>date</th>
                                                <th>username</th>
                                                <?php 
                                                $cek = $_SESSION["username"];
                                                echo $cek;
                                                 ?>
                                                

                                                <?php if ($_SESSION["role"] == 'Admin') {?><th>Aksi</th><?php }?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($dataPost as $dB) { ?>
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td><?php echo $dB->title ?></td>
                                                    <td><?php echo $dB->content ?></td>
                                                    <td><?php echo $dB->date ?></td>
                                                    <td><?php echo $dB->username ?></td>
                                                    <?php if ($_SESSION["role"] == 'Admin') {?>
                                                        <td><a href="<?= base_url('admin/editPost/') ?><?php echo $dB->idpost ?>" class="btn btn-warning btn-circle btn-sm"><i class="fa fa-edit"></i></a>
                                                            <a href="<?= base_url('admin/hapusPost/')?><?php echo $dB->idpost ?>" class="btn btn-danger btn-circle btn-sm btn-hapus"><i class="fa fa-trash"></i></a></td><?php }?>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">                                
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="copyright">
                                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END MAIN CONTENT-->
                            <!-- END PAGE CONTAINER-->
                        </div>

                    </div>

                    <!-- Jquery JS-->
                    <script src="<?php echo base_url().'assets/vendor/jquery-3.2.1.min.js '?>"></script>
                    <!-- Bootstrap JS-->
                    <script src="<?php echo base_url().'assets/vendor/bootstrap-4.1/popper.min.js '?>"></script>
                    <script src="<?php echo base_url().'assets/vendor/bootstrap-4.1/bootstrap.min.js '?>"></script>
                    <!-- Vendor JS       -->
                    <script src="<?php echo base_url().'assets/vendor/slick/slick.min.js '?>">
                    </script>
                    <script src="<?php echo base_url().'assets/vendor/wow/wow.min.js '?>"></script>
                    <script src="<?php echo base_url().'assets/vendor/animsition/animsition.min.js '?>"></script>
                    <script src="<?php echo base_url().'assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js '?>">
                    </script>
                    <script src="<?php echo base_url().'assets/vendor/counter-up/jquery.waypoints.min.js '?>"></script>
                    <script src="<?php echo base_url().'assets/vendor/counter-up/jquery.counterup.min.js '?>">
                    </script>
                    <script src="<?php echo base_url().'assets/vendor/circle-progress/circle-progress.min.js '?>"></script>
                    <script src="<?php echo base_url().'assets/vendor/perfect-scrollbar/perfect-scrollbar.js '?>"></script>
                    <script src="<?php echo base_url().'assets/vendor/chartjs/Chart.bundle.min.js '?>"></script>
                    <script src="<?php echo base_url().'assets/vendor/select2/select2.min.js '?>"></script>
                    <!-- Main JS-->
                    <script src="<?php echo base_url().'assets/js/main.js '?>"></script>

                    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
                    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
                    <script>
                        $(document).ready( function () {
                            $('#table_id').DataTable();
                        } );
                    </script>

                    <script>
                        const flashData = $('.flash-data').data('flashdata');

                        if (flashData) {
                            Swal.f({
                                title : 'Data',
                                text : 'Berhasil'+flashData,
                                type : 'success'
                            });
                        }
                    </script>

                <!-- <script>
                    $(document). on('click', '.btn-hapus', function(e){

                        e.preventDefault();
                        const href = $(this).attr('href');

                        Swal.fire({
                            title: 'Are you sure?',
                            text: "You won't be able to revert this!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, delete it!'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href= href;
                            }
                        })
                    })
                </script>

                <script>
                    const swal = $('.swal').data('swal');
                    if (swal){
                        Swal.fire({
                            title : 'Data Berhasil',
                            text : swal,
                            icon : 'success'
                        })
                    }
                </script> -->

                <!-- <script>
                    function hapus(id) {
                        Swal.fire({
                            title: 'Are you sure?',
                            text: "You won't be able to revert this!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, delete it!'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href="<?php echo site_url('admin/hapusBarang') ?>"/+id;
                            }
                        })
                    }
                </script> -->

            </body>

            </html>
            <!-- end document-->
