<?php $this->load->view('layout/sidebar'); ?>
<?php $this->load->view('layout/header'); ?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<!-- MAIN CONTENT-->
<div class="main-content">
	<div class="section__content section__content--p30">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 mb-4">
					<div class="overview-wrap">
						<h2 class="title-1">Data Post</h2>
					</div>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-lg-11">
					<div class="card">
						<div class="card-header">
							<strong>Form Edit Post</strong>
						</div>
						<div class="card-body card-block">
							<form action="<?= base_url('admin/aksi_editPost/') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
								<?php foreach ($dataPost as $dB) {?>
								<input type="text" name="idpost" value="<?php echo $dB->idpost ?>" hidden="">
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="text-input" class=" form-control-label">Nama Title</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="text" id="text-input" name="title" placeholder="Masukkan Nama Post" class="form-control" value="<?php echo $dB->title ?>">
										<small class="form-text text-muted"><i>*Text</i></small>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="text-input" class=" form-control-label">Konten</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="text"  id="rupiah" name="content" value="<?php echo $dB->content ?>"" placeholder="Masukkan Harga Post" class="form-control">
										<small class="form-text text-muted"><i>*konten</i></small>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="text-input" class=" form-control-label">Date</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="datetime-local" id="text-input"  name="date" placeholder="Masukkan Jumlah Minimal Stok Tersedia" value="<?php echo $dB->date ?>" class="form-control">
										<small class="form-text text-muted"><i>*date</i></small>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="text-input" class=" form-control-label">Username</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="text" id="text-input"  name="jumlah" readonly="" placeholder="Masukkan Jumlah Post" value="<?php echo $dB->username ?>" class="form-control" >
										<small class="form-text text-muted"><i>*Username</i></small>
									</div>
								</div>
								<?php } ?>
							</div>
							<div class="card-footer">
								<button type="submit" class="btn btn-primary btn-sm">
									<i class="fa fa-dot-circle-o"></i> Submit
								</button>
								<button type="reset" class="btn btn-danger btn-sm">
									<i class="fa fa-ban"></i> Reset
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			

			<!-- Jquery JS-->
			<script src="<?php echo base_url().'assets/vendor/jquery-3.2.1.min.js '?>"></script>
			<!-- Bootstrap JS-->
			<script src="<?php echo base_url().'assets/vendor/bootstrap-4.1/popper.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/bootstrap-4.1/bootstrap.min.js '?>"></script>
			<!-- Vendor JS       -->
			<script src="<?php echo base_url().'assets/vendor/slick/slick.min.js '?>">
			</script>
			<script src="<?php echo base_url().'assets/vendor/wow/wow.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/animsition/animsition.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js '?>">
			</script>
			<script src="<?php echo base_url().'assets/vendor/counter-up/jquery.waypoints.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/counter-up/jquery.counterup.min.js '?>">
			</script>
			<script src="<?php echo base_url().'assets/vendor/circle-progress/circle-progress.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/perfect-scrollbar/perfect-scrollbar.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/chartjs/Chart.bundle.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/select2/select2.min.js '?>"></script>
			<!-- Main JS-->
			<script src="<?php echo base_url().'assets/js/main.js '?>"></script>

		</body>

		</html>
		<!-- end document-->
