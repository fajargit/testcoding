<?php $this->load->view('layout/sidebar'); ?>
<?php $this->load->view('layout/header'); ?>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<!-- MAIN CONTENT-->
<div class="main-content">
	<div class="section__content section__content--p30">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 mb-4">
					<div class="overview-wrap">
						<h2 class="title-1">Data Akun</h2>
						<?php if ($_SESSION["role"] == 'Admin') {?>
							<a href="<?= base_url('admin/tambahAkun/') ?>"><button class="au-btn au-btn-icon au-btn--green">
								<i class="zmdi zmdi-plus"></i> Tambah Akun</button></a>
							<?php }?>
						</div>
					</div>
				</div>

				<div class="flash-data" data-flash=" <?= $this->session->flashdata('flash') ?>" ></div>

				<div class="row">
					<div class="col-lg-12">
						<div class="row">
							<div class="col-lg-12">
								<div class="table-responsive table--no-card m-b-40">
									<table id="table_id" class="table table-borderless table-striped table-earning">
										<thead>
											<tr>
												<th>No</th>
												<th>Username</th>
												<th>Password</th>
												<th>name</th>
												<th>role</th>
												<?php 
												$cek = $_SESSION["username"];
												echo $cek;
												 ?>
												
												<?php if ($_SESSION["role"] == 'Admin') {?><th>Aksi</th><?php }?>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($dataAkun as $dB) { ?>
												<tr>
													<td><?php echo $no++ ?></td>
													<td><?php echo $dB->username ?></td>
													<td><?php echo $dB->password ?></td>
													<td><?php echo $dB->name ?></td>
													<td><?php echo $dB->role ?></td>
													<?php if ($_SESSION["role"] == 'Admin') {?>
														<td><a href="<?= base_url('admin/editAkun/') ?><?php echo $dB->username ?>" class="btn btn-warning btn-circle btn-sm"><i class="fa fa-edit"></i></a>
															<a href="<?= base_url('admin/hapusAkun/')?><?php echo $dB->username ?>" class="btn btn-danger btn-circle btn-sm btn-hapus"><i class="fa fa-trash"></i></a></td><?php }?>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">                                
										<div class="row">
											<div class="col-md-12">
												<div class="copyright">
													<p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- END MAIN CONTENT-->
							<!-- END PAGE CONTAINER-->
						</div>

					</div>

					<!-- Jquery JS-->
					<script src="<?php echo base_url().'assets/vendor/jquery-3.2.1.min.js '?>"></script>
					<!-- Bootstrap JS-->
					<script src="<?php echo base_url().'assets/vendor/bootstrap-4.1/popper.min.js '?>"></script>
					<script src="<?php echo base_url().'assets/vendor/bootstrap-4.1/bootstrap.min.js '?>"></script>
					<!-- Vendor JS       -->
					<script src="<?php echo base_url().'assets/vendor/slick/slick.min.js '?>">
					</script>
					<script src="<?php echo base_url().'assets/vendor/wow/wow.min.js '?>"></script>
					<script src="<?php echo base_url().'assets/vendor/animsition/animsition.min.js '?>"></script>
					<script src="<?php echo base_url().'assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js '?>">
					</script>
					<script src="<?php echo base_url().'assets/vendor/counter-up/jquery.waypoints.min.js '?>"></script>
					<script src="<?php echo base_url().'assets/vendor/counter-up/jquery.counterup.min.js '?>">
					</script>
					<script src="<?php echo base_url().'assets/vendor/circle-progress/circle-progress.min.js '?>"></script>
					<script src="<?php echo base_url().'assets/vendor/perfect-scrollbar/perfect-scrollbar.js '?>"></script>
					<script src="<?php echo base_url().'assets/vendor/chartjs/Chart.bundle.min.js '?>"></script>
					<script src="<?php echo base_url().'assets/vendor/select2/select2.min.js '?>"></script>
					<!-- Main JS-->
					<script src="<?php echo base_url().'assets/js/main.js '?>"></script>

					<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
					<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
					<script>
						$(document).ready( function () {
							$('#table_id').DataTable();
						} );
					</script>


			</body>

			</html>
			<!-- end document-->
