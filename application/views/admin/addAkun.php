<?php $this->load->view('layout/sidebar'); ?>
<?php $this->load->view('layout/header'); ?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<!-- MAIN CONTENT-->
<div class="main-content">
	<div class="section__content section__content--p30">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 mb-4">
					<div class="overview-wrap">
						<h2 class="title-1">Data Akun</h2>
					</div>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-lg-11">
					<div class="card">
						<div class="card-header">
							<strong>Form tambah Akun</strong>
						</div>
						<div class="card-body card-block">
							<form action="<?= base_url('admin/aksi_tambahAkun/') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="text-input" class=" form-control-label">Username</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="text" id="text-input" name="username" placeholder="Masukkan Username" class="form-control">
										<small class="form-text text-muted"><i>*username</i></small>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="text-input" class=" form-control-label">Password</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="password"  id="rupiah" name="password" placeholder="Masukkan Harga Post" class="form-control">
										<small class="form-text text-muted"><i>*password</i></small>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="text-input" class=" form-control-label">Nama</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="text" id="text-input" name="name" placeholder="Masukkan Nama" class="form-control">
										<small class="form-text text-muted"><i>*nama</i></small>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="text-input" class=" form-control-label">Role</label>
									</div>
									<div class="col-12 col-md-9">
										<select name="role"  class="form-control">
											<option>Pilih Role</option>
												<option value="Admin">Admin</option>
												<option value="Author">Author</option>
										</select>
										<small class="form-text text-muted"><i>*pilih</i></small>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<button type="submit" class="btn btn-primary btn-sm">
									<i class="fa fa-dot-circle-o"></i> Submit
								</button>
								<button type="reset" class="btn btn-danger btn-sm">
									<i class="fa fa-ban"></i> Reset
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			

			<!-- Jquery JS-->
			<script src="<?php echo base_url().'assets/vendor/jquery-3.2.1.min.js '?>"></script>
			<!-- Bootstrap JS-->
			<script src="<?php echo base_url().'assets/vendor/bootstrap-4.1/popper.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/bootstrap-4.1/bootstrap.min.js '?>"></script>
			<!-- Vendor JS       -->
			<script src="<?php echo base_url().'assets/vendor/slick/slick.min.js '?>">
			</script>
			<script src="<?php echo base_url().'assets/vendor/wow/wow.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/animsition/animsition.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js '?>">
			</script>
			<script src="<?php echo base_url().'assets/vendor/counter-up/jquery.waypoints.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/counter-up/jquery.counterup.min.js '?>">
			</script>
			<script src="<?php echo base_url().'assets/vendor/circle-progress/circle-progress.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/perfect-scrollbar/perfect-scrollbar.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/chartjs/Chart.bundle.min.js '?>"></script>
			<script src="<?php echo base_url().'assets/vendor/select2/select2.min.js '?>"></script>
			<!-- Main JS-->
			<script src="<?php echo base_url().'assets/js/main.js '?>"></script>

		</body>

		</html>
		<!-- end document-->
